<!-- extends file layouts\main.blade.php dari view -->
@extends("layouts.main")

<!-- Set section child dengan inisial "container" -->
@section("container")
    <h1>Halaman About</h1>
    <!-- Blade Templates, memanggil data yg dikirimkan dari routes -->
    <h3>{{$name}}</h3>
    <p>{{$email}}</p>
@endsection