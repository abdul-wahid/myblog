<!-- extends file layouts\main.blade.php dari view -->
@extends("layouts.main")

<!-- Set section child dengan inisial "container" -->
@section("container")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>{{ $post->title }}</h2>
                {{-- $post->kategori->name: panggil field name berdasarkan relasi ke kategori --}}
                <p>
                    By. <a href="/blog?user={{ $post->user->username }}" class="text-decoration-none">{{ $post->user->name }}</a> 
                    in 
                    <a href="/blog?kategori={{ $post->kategori->slug }}" class="text-decoration-none">{{ $post->kategori->name }}</a>
                </p>

                @if ($post->image)
                    {{-- asset("storage/" . $post->image): panggil file dari public/storage/namafile --}}
                    <div style="max-height: 350px; overflow: hidden">
                        <img src="{{ asset("storage/" . $post->image) }}" alt="{{ $post->kategori->name }}" class="img-fluid">
                    </div>
                @else
                    <img src="https://source.unsplash.com/1200x400?{{ $post->kategori->name }}" alt="{{ $post->kategori->name }}" class="img-fluid">
                @endif

                <article class="my-3">
                    {{-- Tampil data dengan format html --}}
                    {!! $post->body !!}
                </article>

            
                <a href="/blog" class="text-decoration-none mt-3 d-block">Back to Posts</a>
            </div>
        </div>
    </div>
        
@endsection