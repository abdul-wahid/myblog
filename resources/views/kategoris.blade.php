<!-- extends file layouts\main.blade.php dari view -->
@extends("layouts.main")

<!-- Set section child dengan inisial "container" -->
@section("container")
    <h1 class="mb-3">{{ $title }}</h1>

    <div class="container">
        <div class="row">
            <!-- Blade Templates - foreach, memanggil data yg dikirimkan dari routes -->
            @foreach($kategoris as $kategori)
            <div class="col-md-4">
                <a href="/blog?kategori={{ $kategori->slug }}">
                    <div class="card bg-dark text-white">
                        <img src="https://source.unsplash.com/500x500?{{ $kategori->name }}" class="card-img" alt="{{ $kategori->name }}">
                        <div class="card-img-overlay d-flex align-items-center p-0">
                            <h3 class="card-title text-center flex-fill p-4" style="background-color: rgba(0, 0, 0, 0.5)">{{ $kategori->name }}</h3>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
@endsection