@extends('dashboard.layouts.main')

@section('container')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Post Categories</h1>
    </div>

    @if (session()->has("success"))
    <div class="alert alert-success col-lg-8" role="alert">
        {{ session("success") }}
    </div>
    @endif

    <div class="table-responsive">
        <a href="/dashboard/kategoris/create" class="btn btn-primary mb-3">Create Category</a>
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Category Name</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($kategoris as $kategori)
              <tr>
                  {{-- $loop->iteration: perulangan dimulai dari 1 --}}
                <td>{{ $loop->iteration }}</td>
                <td>{{ $kategori->name }}</td>
                <td>
                    {{-- /dashboard/posts/{{ $post->id }}: otomatis menjalankan function "show" pada controller DashboardPostController --}}
                    <a href="/dashboard/kategoris/{{ $kategori->slug }}" class="badge bg-info"><span data-feather="eye"></span></a>
                    <a href="/dashboard/kategoris/{{ $kategori->slug }}/edit" class="badge bg-warning"><span data-feather="edit"></span></a>
                    <form action="/dashboard/kategoris/{{ $kategori->slug }}" method="POST" class="d-inline">
                      @method("delete")
                      @csrf
                      <button class="badge bg-danger border-0" onclick="return confirm(`Are you sure?`)"><span data-feather="x-circle"></span></button>
                    </form>
                </td>
              </tr> 
              @endforeach
          </tbody>
        </table>
    </div>
@endsection