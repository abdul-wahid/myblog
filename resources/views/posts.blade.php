<!-- extends file layouts\main.blade.php dari view -->
@extends("layouts.main")

<!-- Set section child dengan inisial "container" -->
@section("container")
    <h1 class="mb-3 text-center">{{ $title }}</h1>

    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <form action="/blog">
                @if (request("kategori"))
                    <input type="hidden" name="kategori" value="{{ request("kategori") }}">
                @endif
                
                @if (request("user"))
                    <input type="hidden" name="user" value="{{ request("user") }}">
                @endif

                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search.." name="search" value="{{ request("search") }}">
                    <button class="btn btn-danger" type="submit">Search</button>
                </div>
            </form>
        </div>
    </div>

    @if ($posts->count())
        <div class="card mb-3">
            @if ($posts[0]->image)
            {{-- asset("storage/" . $posts[0]->image): panggil file dari public/storage/namafile --}}
            <div style="max-height: 350px; overflow: hidden">
                <img src="{{ asset("storage/" . $posts[0]->image) }}" alt="{{ $posts[0]->kategori->name }}" class="card-img-top">
            </div>
            @else
            <img src="https://source.unsplash.com/1200x400?{{ $posts[0]->kategori->name }}" class="card-img-top" alt="{{ $posts[0]->kategori->name }}">
            @endif
            <div class="card-body text-center">
                <h3 class="card-title"><a href="/posts/{{ $posts[0]->slug }}" class="text-decoration-none text-dark">{{ $posts[0]->title }}</a></h3>
                <p>
                    <small class="text-muted">
                        By. <a href="/blog?user={{ $posts[0]->user->username }}" class="text-decoration-none">{{ $posts[0]->user->name }}</a>
                        in
                        <a href="/blog?kategori={{ $posts[0]->kategori->slug }}" class="text-decoration-none">{{ $posts[0]->kategori->name }}</a>
                        
                        {{-- diffForHumans(): selisih waktu antara waktu tertentu dengan waktu sekarang --}}
                        {{ $posts[0]->created_at->diffForHumans() }}
                    </small>
                </p>
                <p class="card-text">{{ $posts[0]->excerpt }}</p>
                <a href="/posts/{{ $posts[0]->slug }}" class="text-decoration-none btn btn-primary">Read more</a>
            </div>
        </div>

        <div class="container">
            <div class="row">
                
                <!-- 
                    Blade Templates - foreach, memanggil data yg dikirimkan dari routes
                    skip(1): lewati satu data 
                -->
                @foreach ($posts->skip(1) as $post)
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="position-absolute px-3 py-2 text-white" style="background-color: rgba(0, 0, 0, 0.5)">
                            <a href="/blog?kategori={{ $post->kategori->slug }}" class="text-white text-decoration-none">{{ $post->kategori->name }}</a>
                        </div>
                        @if ($post->image)
                        <img src="{{ asset("storage/" . $post->image) }}" alt="{{ $post->kategori->name }}" class="card-img-top">
                        @else
                        <img src="https://source.unsplash.com/500x400?{{ $post->kategori->name }}" class="card-img-top" alt="{{ $post->kategori->name }}">
                        @endif
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p>
                                <small class="text-muted">
                                    By. <a href="/blog?user={{ $post->user->username }}" class="text-decoration-none">{{ $post->user->name }}</a>
                                    
                                    {{-- diffForHumans(): selisih waktu antara waktu tertentu dengan waktu sekarang --}}
                                    {{ $post->created_at->diffForHumans() }}
                                </small>
                            </p>
                            <p class="card-text">{{ $post->excerpt }}</p>
                            <a href="/posts/{{ $post->slug }}" class="btn btn-primary">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    @else
        <p class="text-center fs-4">No post found.</p>
    @endif

    <div class="d-flex justify-content-end">
        {{-- Tampil tombol pagination --}}
        {{ $posts->links() }}
    </div>

@endsection