<?php
// Menghubungkan ke model tertentu
use App\Models\Kategori;
use App\Models\User;

use Illuminate\Support\Facades\Route;

// Menghubungkan ke controller tertentu
use App\Http\Controllers\PostController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardPostController;
use App\Http\Controllers\AdminKategoriController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("home", [
        "title" => "Home",
        "active" => "home"
    ]);
});

Route::get('/about', function () {
    // Parameter 1: Memanggil file about.blade.php pada view
    // Parameter 2: data yg dikirimkan
    return view("about", [
        "title" => "About",
        "active" => "about",
        "name" => "Abdul Wahid",
        "email" => "alamatemail@email.com"
    ]);
});


Route::get('/blog', [
    // Memanggil function index pada controller PostController
    PostController::class, "index"
]);

// {post}: data by PK
// {post:slug}: data by field slug
Route::get("/posts/{post:slug}", [
    PostController::class, "show"
]);

Route::get("/kategoris", function () {
    return view("kategoris", [
        "title" => "Post Kategoris",
        "active" => "kategoris",
        "kategoris" => Kategori::all()
    ]);
});

Route::get('/kategoris/{by:slug}', function (Kategori $by) {
    return view("posts", [
        "title" => "Post By Kategori : $by->name",
        "active" => "kategoris",

        // load("user", "kategori"): panggil function relasi tabel dari model POST
        // biasa digunakan ketika relasi tabel one to many (hasMany)
        // Jika di bentukan query seperti SELECT * FROM namatabel, per relasi table
        // hanya berlaku untuk tampil lebih dari satu dengan memanggil relasi tabel

        // posts: panggil function relasi tabel dari mobel Kategori

        "posts" => $by->posts->load("user", "kategori")
    ]);
});

Route::get("/authors/{user:username}", function (User $user) {
    return view("posts", [
        "title" => "Post By Author : $user->name",
        "posts" => $user->posts->load("kategori", "user")
    ]);
});

// name("login"): memberi tahu bahwa ini adalah url untuk halaman login
// middleware("guest"): menandakan hak akses yg belum login (tamu)
Route::get("/login", [LoginController::class, "index"])->name("login")->middleware("guest");

Route::post("/login", [LoginController::class, "authenticate"]);

Route::post("/logout", [LoginController::class, "logout"]);

Route::get("/register", [RegisterController::class, "index"])->middleware("guest");

Route::post("/register", [RegisterController::class, "store"]);

// middleware("auth"): menandakan hak akses yg sudah login
Route::get("/dashboard", function () {
    return view("dashboard.index");
})->middleware("auth");

Route::get("/dashboard/posts/checkSlug", [DashboardPostController::class, "checkSlug"])->middleware("auth");

// resource: menangani fungsi CRUD sesuai dengan nama controller yg sudah di buat dengan menggunakan resource
// contoh, jika menjalankan url "/dashboard/posts" otomatis akan menjalankan function "index"
Route::resource("/dashboard/posts", DashboardPostController::class)->middleware("auth");

// except("show"): tidak dapat mengakses function show
Route::resource("/dashboard/kategoris", AdminKategoriController::class)->except("show")->middleware("admin");
