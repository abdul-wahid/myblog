<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Kategori;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Abdul Wahid",
            "username" => "abdulwahid",
            "email" => "abdulwahid@gmail.com",
            "password" => bcrypt("password")
        ]);

        // User::create([
        //     "name" => "Zaki Prasasti",
        //     "email" => "zakiprasasti@gmail.com",
        //     "password" => bcrypt("12345")
        // ]);

        // membuat data baru sesuai yg di set di database\factories\UserFactory.php pada function definition
        User::factory(3)->create();
        
        Kategori::create([
            "name" => "Web Programming",
            "slug" => "web-programming"
        ]);

        Kategori::create([
            "name" => "Web Design",
            "slug" => "web-design"
        ]);
        
        Kategori::create([
            "name" => "Personal",
            "slug" => "personal"
        ]);
        
        Post::factory(20)->create();
        
        // Post::create([
        //     "title" => "Judul Pertama",
        //     "slug" => "judul-pertama",
        //     "excerpt" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam.",
        //     "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam. Vel, ipsa temporibus, obcaecati sed maiores exercitationem reiciendis excepturi quibusdam, accusantium necessitatibus porro. Accusantium eius laudantium repellat neque illo suscipit doloremque. Ad voluptatibus earum dicta sequi quos laudantium aspernatur debitis asperiores. Dolorum iure exercitationem vero, doloremque debitis, soluta quaerat voluptates eius non aliquam illum culpa eaque ut similique sequi numquam odit ducimus necessitatibus magni. Temporibus optio tempora, nam odit doloremque velit. Sint, ipsum minima? Consequuntur, doloribus voluptates ab cumque placeat et labore voluptas nulla officia ratione non suscipit nostrum ea pariatur, architecto fuga deleniti praesentium?",
        //     "kategori_id" => 1,
        //     "user_id" => 1
        // ]);

        // Post::create([
        //     "title" => "Judul Kedua",
        //     "slug" => "judul-kedua",
        //     "excerpt" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam.",
        //     "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam. Vel, ipsa temporibus, obcaecati sed maiores exercitationem reiciendis excepturi quibusdam, accusantium necessitatibus porro. Accusantium eius laudantium repellat neque illo suscipit doloremque. Ad voluptatibus earum dicta sequi quos laudantium aspernatur debitis asperiores. Dolorum iure exercitationem vero, doloremque debitis, soluta quaerat voluptates eius non aliquam illum culpa eaque ut similique sequi numquam odit ducimus necessitatibus magni. Temporibus optio tempora, nam odit doloremque velit. Sint, ipsum minima? Consequuntur, doloribus voluptates ab cumque placeat et labore voluptas nulla officia ratione non suscipit nostrum ea pariatur, architecto fuga deleniti praesentium?",
        //     "kategori_id" => 1,
        //     "user_id" => 1
        // ]);

        // Post::create([
        //     "title" => "Judul Ketiga",
        //     "slug" => "judul-ketiga",
        //     "excerpt" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam.",
        //     "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam. Vel, ipsa temporibus, obcaecati sed maiores exercitationem reiciendis excepturi quibusdam, accusantium necessitatibus porro. Accusantium eius laudantium repellat neque illo suscipit doloremque. Ad voluptatibus earum dicta sequi quos laudantium aspernatur debitis asperiores. Dolorum iure exercitationem vero, doloremque debitis, soluta quaerat voluptates eius non aliquam illum culpa eaque ut similique sequi numquam odit ducimus necessitatibus magni. Temporibus optio tempora, nam odit doloremque velit. Sint, ipsum minima? Consequuntur, doloribus voluptates ab cumque placeat et labore voluptas nulla officia ratione non suscipit nostrum ea pariatur, architecto fuga deleniti praesentium?",
        //     "kategori_id" => 2,
        //     "user_id" => 1
        // ]);

        // Post::create([
        //     "title" => "Judul Keempat",
        //     "slug" => "judul-keempat",
        //     "excerpt" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam.",
        //     "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, in laborum. Consectetur numquam at nam. Vel, ipsa temporibus, obcaecati sed maiores exercitationem reiciendis excepturi quibusdam, accusantium necessitatibus porro. Accusantium eius laudantium repellat neque illo suscipit doloremque. Ad voluptatibus earum dicta sequi quos laudantium aspernatur debitis asperiores. Dolorum iure exercitationem vero, doloremque debitis, soluta quaerat voluptates eius non aliquam illum culpa eaque ut similique sequi numquam odit ducimus necessitatibus magni. Temporibus optio tempora, nam odit doloremque velit. Sint, ipsum minima? Consequuntur, doloribus voluptates ab cumque placeat et labore voluptas nulla officia ratione non suscipit nostrum ea pariatur, architecto fuga deleniti praesentium?",
        //     "kategori_id" => 2,
        //     "user_id" => 2
        // ]);
    }
}
