<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // panggil librari faker php
            // Dokumentasi faker php: https://fakerphp.github.io/
            // mt_rand(2,8): radom angka dari 2 sampai 8
            "title" => $this->faker->sentence(mt_rand(2,8)),
            "slug" => $this->faker->slug(),
            "excerpt" => $this->faker->paragraph(),
            // "body" => "<p>" . implode("</p><p>", $this->faker->paragraphs(mt_rand(5, 10))) . "</p>",
            "body" => collect($this->faker->paragraphs(mt_rand(5, 10)))
                ->map(fn($p) => "<p>$p</p>")
                ->implode(""),
            "user_id" => mt_rand(1, 4),
            "kategori_id" => mt_rand(1, 3)
        ];
    }
}
