<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use Sluggable, HasFactory;

    // Field mana saja yg boleh diisi oleh user
    // protected $fillable = ["title", "excerpt", "body"];

    // Field mana saja yg tidak boleh diisi oleh user
    protected $guarded = ["id"];

    // biasa digunakan ketika relasi tabel one to one (belongsTo)
    // $with = ["kategori", "user"]: panggil function relasi table dari model POST
    // Jika di bentukan query seperti SELECT * FROM namatabel, per relasi table
    protected $with = ["kategori", "user"];

    // nama function "getRouteKeyName" adalah bawaan dari Laravel
    // untuk setiap route pencarian data berdasarkan field "slug"
    public function getRouteKeyName(){
        return "slug";
    }

    // Relasi tabel
    public function kategori(){
        // Menghubungkan ke tabel kategoris.id = posts.kategori_id
        return $this->belongsTo(Kategori::class, "kategori_id");
    }

    public function user(){
        return $this->belongsTo(User::class, "user_id");
    }

    // kata-kata "scope" wajib ada setelah itu bisa bebas
    // scopeFilter($query, $filters = []): params1 yaitu default callback, params2 nama params pada method GET
    public function scopeFilter($query, $filters = []){
        $query->when($filters["search"] ?? false, function($query, $search){
            return $query->where(function($query) use ($search){
                $query->where("title", "like", "%" . $search . "%")
                    ->orWhere("body", "like", "%" . $search . "%");
            });
        });

        $query->when($filters["kategori"] ?? false, function($query, $kategori){
            // whereHas("kategori": panggil function ralasi tabel kategori
            return $query->whereHas("kategori", function($query) use ($kategori){
                $query->where("slug", $kategori);
            });
        });

        $query->when($filters["user"] ?? false, fn($query, $user) =>
            // whereHas("user": panggil function ralasi tabel user
            $query->whereHas("user", fn($query) =>
                $query->where("username", $user)
            )
        );
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                // 'title': nama field pada table Post
                'source' => 'title'
            ]
        ];
    }
}
