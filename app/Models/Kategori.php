<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public function posts(){
        // relasi tabel ke post otomatis relasi dari kategori.id = post.kategori_id
        return $this->hasMany(Post::class);
    }
}
