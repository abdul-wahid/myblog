<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    public function index(){
        return view("register.index", [
            "title" => "Register",
            "active" => "register"
        ]);
    }

    public function store(Request $req){
        // validate: memberikan validasi pada inputan
        // Jika tidak lolos validate maka tidak akan menjalankan proses di bawahnya
        $validatedData = $req->validate([
            // "name": nama inputan
            "name" => "required|max:255",
            // unique:users: memeriksa pada tabel users.username tidak boleh sama
            "username" => ["required", "min:3", "max:255", "unique:users"],
            "email" => "required|email:dns|unique:users",
            "password" => "required|min:5|max:255"
        ]);

        // bcrypt: mengenkripsi inputan user 
        $validatedData["password"] = bcrypt($validatedData["password"]);

        // User: nama model
        // create: buat data
        User::create($validatedData);

        // redirect: menjalankan route/web by url yg di tuju
        // with: set flash data dengan nama "success"
        return redirect("/login")->with("success", "Registration seccessfull!");
    }
}
