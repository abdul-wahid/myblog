<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Models\Kategori;

class PostController extends Controller
{
    public function index()
    {
        $title = "";

        if (request("kategori")) {
            $kategori = Kategori::firstWhere("slug", request("kategori"));
            $title = " in $kategori->name";
        }

        if (request("user")) {
            $user = User::firstWhere("username", request("user"));
            $title = " by $user->name";
        }

        return view("posts", [
            "title" => "All Posts" . $title,
            "active" => "posts",

            // Tampil semua data
            // "posts" => Post::all()

            // latest(): by created_at terbaru
            // filter(): panggil function scope bernama filter pada model Post
            // get(): ambil data
            // paginate(7): tampil per 7 row data
            // withQueryString(): agar row data hasil pencarian tidak terhapus ketika klik paginatin 
            "posts" => Post::latest()->filter(request(["search", "kategori", "user"]))->paginate(7)->withQueryString()
        ]);
    }

    // Params1: nama model, params2: data by slug
    public function show(Post $post)
    {
        return view("post", [
            "title" => "Single Post",
            "active" => "posts",

            // menimpa $post menjadi 1 row data Post by slug
            "post" => $post
        ]);
    }
}
