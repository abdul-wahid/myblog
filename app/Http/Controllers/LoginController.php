<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        // mengarah ke resources\views\login\index.blade.php
        return view("login.index", [
            "title" => "Login",
            "active" => "login"
        ]);
    }

    public function authenticate(Request $req){
        $credentials = $req->validate([
            "email" => ["required", "email"],
            "password" => ["required"]
        ]);

        // mencocokan antara (inputan email dan password) dan (email dan password pada tabel users) 
        if(Auth::attempt($credentials)){
            // generate ulang id session dan masuk ke halaman selanjutnya
            $req->session()->regenerate();
            return redirect()->intended("/dashboard");
        }

        // jika tidak cocok maka, balik ke halaman yg sama dengan mengirimkan flash data
        return back()->with("loginError", "Login failed!");
    }

    public function logout(){
        // proses logout
        Auth::logout();
 
        // mematika id session
        request()->session()->invalidate();
    
        // membuat ulang id session
        request()->session()->regenerateToken();
    
        return redirect('/');
    }
}
