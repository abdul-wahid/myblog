<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Tentang Aplikasi

Aplikasi ini di buat dengan framework php Laravel 8 dengan tema bloger menggunakan database MySql dengan nama database blog-db

##

Jalankan perintah ini untuk memasukan data pada database

```json
php artisan migrate:fresh --seed
```

##

### Contoh User

Email: abdulwahid@gmail.com, Password: password
